!
version 12.2
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname BatB1
!
!
!
!
!
!
spanning-tree mode rapid-pvst
spanning-tree portfast default
spanning-tree portfast bpduguard default
spanning-tree extend system-id
spanning-tree vlan 10,20,30,40,50,60,70,80,100 priority 8192
!
interface Port-channel1
 switchport trunk native vlan 100
 switchport trunk allowed vlan 10,20,30,40,50,60,70,80,100
 switchport mode trunk
 spanning-tree portfast disable
!
interface Port-channel2
 switchport trunk native vlan 100
 switchport trunk allowed vlan 10,20,30,40,50,60,70,80,100
 switchport mode trunk
 spanning-tree portfast disable
!
interface FastEthernet0/1
 switchport access vlan 50
 switchport mode access
 switchport voice vlan 30
 spanning-tree portfast
 spanning-tree bpduguard enable
!
interface FastEthernet0/2
 switchport access vlan 60
 switchport mode access
 spanning-tree portfast
 spanning-tree bpduguard enable
!
interface FastEthernet0/3
!
interface FastEthernet0/4
!
interface FastEthernet0/5
!
interface FastEthernet0/6
!
interface FastEthernet0/7
!
interface FastEthernet0/8
!
interface FastEthernet0/9
!
interface FastEthernet0/10
!
interface FastEthernet0/11
!
interface FastEthernet0/12
!
interface FastEthernet0/13
!
interface FastEthernet0/14
!
interface FastEthernet0/15
!
interface FastEthernet0/16
!
interface FastEthernet0/17
!
interface FastEthernet0/18
!
interface FastEthernet0/19
!
interface FastEthernet0/20
!
interface FastEthernet0/21
!
interface FastEthernet0/22
!
interface FastEthernet0/23
 switchport trunk native vlan 100
 switchport trunk allowed vlan 10,20,30,40,50,60,70,80,100
 switchport mode trunk
 channel-group 1 mode passive
!
interface FastEthernet0/24
 switchport trunk native vlan 100
 switchport trunk allowed vlan 10,20,30,40,50,60,70,80,100
 switchport mode trunk
 channel-group 1 mode passive
!
interface GigabitEthernet0/1
 switchport trunk native vlan 100
 switchport trunk allowed vlan 10,20,30,40,50,60,70,80,100
 switchport mode trunk
 channel-group 2 mode active
!
interface GigabitEthernet0/2
 switchport trunk native vlan 100
 switchport trunk allowed vlan 10,20,30,40,50,60,70,80,100
 switchport mode trunk
 channel-group 2 mode active
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

