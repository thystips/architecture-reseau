=  TP Conception
Lucas Requena; Antoine Thys

== Proposition

Nous vous proposons donc de remplacer votre matériel existant par des switchs plus récent supportant des liens 10Go. Nous allons mettre en place une architecture 3 tiers sur votre 

Chaque lien entre les switchs et les `DC` possèdents 2 fibres en agrégation afin de doubler la bande passante et permettre une redondance des liens.

Les liaisons entre les switchs sont faites en fibre sauf pour ceux du cœur de réseau et du `bâtiment B` qui sont stackés c'est à dire que 2 équipements physiques sont considérés comme un seul équipement logique avec une gestion unique.

Dans le cas d'une coupure au niveau du `bâtiment A`, le `PCA` du `bâtiment C` pourra prendre le relai.

Pour la sécurité de votre réseau nous avons prévu d'installer plusieus types de pare-feux que nous détaillerons au fil du document.

Afin de permettre une connexion entre vos sites distants, le siège de l'entreprise que vous venez de racheter et le siège actuel nous allons mettre en place un VPN de type `SSL` pour les travailleurs distants (télétravail ou déplacement professionnel). Ensuite pour l'accès au nouveau datacenter nous vous proposons deux solution, la mise en place d'un Tunnel `IPsec` (un vpn de type site à site) ou un MPLS côté opérateur. La mise en place de ces deux solutions en parallèle permettrait une tolérance aux pannes soit du tunnel soit côté opérateur. Le VPN `IPsec` permettra également aux sites distants d'avoir accès au ressources du datacenter comme les serveurs de fichiers.

== Schéma

=== Siège

Voici un schéma du réseau du siège :

image::images/Schema_reseau.svg[]

=== DataCenter

Voici un schéma détaillant le réseau du datacenter :

image::images/Schema_reseau_datacenter.svg[]

=== Réseau externe

Voici un schéma représentant les liaisons externes pour les différents types de sites :

image::images/Schema_WAN.svg[]

=== Sites administratifs

Pour les sites administratifs nous vous proposons cette organisation du réseau :

image::images/Schema_administratif.svg[]

=== Sites industriels

Pour les sites industriels nous allons isolé la partie bureau et la partie industriel au niveau 2 en utilisant des ports différents ports sur le pare-feu :

image::images/Schema_industriel.svg[]

== Siège de la nouvelle acquisition

Afin de permettre une communication entre le siège de l'entreprise existante nous vous proposons de mettre en place un tunnel `IPSec` via la connexion WAN et de demander à votre opérateur la mise en place d'un MPLS. +
Ces technologies pourra être mises en place sur les pare-feux actuellement présents.

== Nouveau sites distants

=== Sites administratifs

Pour les sites administratifs une connexion VPN sera mise en place afin d'accéder au ressources du datacenter. Le SD-WAN nous facilitera la gestion de ces équipements. +
Pour les sites plus sensibles ayant besoin d'une grande tolérance de pannes vous aurez la possibilité de d'ajouter une deuxième pare-feu pour faire de la haute disponibilité. Une fois livré sur site une personne ayant accès au matériel pourra raccorder le nouvel équipement qui se configurera automatiquement. +
De notre côté nous nous engageons à vous effectuer l'ajout préalable de le système de gestion dans le cadre d'un contrat de maintenant. Ceci est également valable en cas d'aquisition de nouveaux batiments.

=== Sites industriels

En suivant les recommandations de l'ANSSI nous avons choisi d'isoler le réseau contenants les machines industriel du réseau administratif des usines. Afin de permettre une gestion des ces équipements nous allons autorisé le vlan `admin` du siège à accéder à ces machines. +
Cette solution permettra de pouvoir effectuer des actions sur l'équipement depuis le siège mais uniquement pour le personnel autorisé via le vlan `admin`. Afin de rendre ceci possible avec un impacte minimal sur la sécurité tout le traffic des zones industrielles passeront uniquement par le `MPLS` et ces machines n'auront donc pas accès à internet.

== VPN

=== SSL

Ce VPN permettra au travailleurs qui se trouvent en dehors des locaux de l'entreprise d'avoir accès aux ressources nécessaire à leur travail.

=== IPsec

Ce type de vpn quant à lui va permettre une connexion transparentes et sécurisé des deux datacenter comme s'ils se trouvaient sur le même réseau afin de pouvoir partager leurs ressources.
Il permettra également à vos sites distants de communiquer avec les ressources des datacenters.

=== MPLS

Avec un `MPLS` vous avez un lien entre les deux datacenter grâce à un routage privatif sur les infrastructures de votre ISP.

== SD-WAN

Nous allons également mettre en place du `SD-WAN` grâce à cela nous allons avoir un visuel sur les pare-feux des différents sites et permettre un facilité d'installation. En effet en cas d'aquisition d'un nouveau site vous pourrez envoyer un nouveau pare-feu sur site avec une simple documentation d'installation accessible pour du personnel sans formation spécialisée. Vous ferrez donc un éconnomie sur les coûts d'installations.

== Vlans

Nous avons établis une liste des Vlans nécessaires pour cette configuration :

* **Vlan 10 (admin)**: Priorité en QoS et accès a tout les équipements
* **Vlan 20 (network)**: Administration des équipements réseau
* **Vlan 30 (voip)**: Tout les téléphones sont dessus
* **Vlan 40 (serveur)**:Tout les serveurs sont dessus exepté les serveurs sensibles
* **Vlan 50 (Serveurs Sensibles)**: Il s'agit d'un Vlan dédié pour les serveurs plus sensibles comme les serveurs de sauvegarde.
* **Vlan 60 (PC)**: Tout les pc sont dessus
* **Vlan 70 (imprimante)**: Toutes les imprimantes sont dessus
* **Vlan 80 (wifi)**: Le wi-fi passe par ce vlan
* **Vlan 90 (invité)**: vlan suplémentaire pour invités
* **Vlan 100 (NATIVE)**: Vlan natif pour la communication entre les équipements

Nous avons ordonnés cette liste de façon à respecter les priorités QOS que nous vous recommandons.

== Adressage

Pour l'adressage des différents sites nous avons choisi d'utiliser la convention suivante : 

* **`10.x.y.0/24`** pour les différents réseaux :
** où **`x`** est le numéro attribué au site
** et **`y`** est le numéro correspondant au vlan
* **`10.x.y.z`** pour les machines :
** où **`z`** est compris entre 1 et 250

=== Réseau spécifiques

Pour les VPNs les réseau de tunnels utilisées sont :

* **`10.254.0.0/16`** : Pour l'IPsec
* **`10.253.0.0/16`** : Pour le VPN SSL

== Pare-feux

Voici les différents types de pare-feux utilisés :

=== NGFW

Les `NGFW` sont les pare-feux qui sont reliés au modems de vos 2 `ISP` et fournissent l'accès à Internet. +
Ils vont aussi filtrer tout le traffic sortant et fournir les différents types de VPNs (IPsec/SSL). +

=== DCFW

Les `DCFW` protègent les équipement du datacenter de divers menaces comme les attaques `DDOS`.

=== ISFW

Les pare-feux `ISFW` vont protéger les équipements clients au sein de l'entreprise. Il s'agit également de l'équipement qui va gérer le routage inter-vlans et permettre de restreindre l'accès à certains périphériques comme les imprimantes.

=== UTM

Les `UTM` seront installés sur les sites industrialisés de votre entreprise, il s'agit en fait de `NGFW` avec des modules spécifiques comme des ports PoE ou des bornes sans fils.

== Routage

Le routage inter-vlans pour effectué par les pare-feux. Pour les différents Vlans des bureaux ce sera via les pare-feux `ISFW`. +
Pour ce qui est de la communication entre les bureaux et le datacenter ainsi que l'extérieur il faudra ajouter des routes.

À cet effet nous vous recommandons le protocol `OSPF` qui est compatible avec la majeur partie des équipements récents. Voici une liste des routes nécessaire :

* **`DCFW` <-> `NGFW`**
* **`ISFW` <-> `NGFW`**
* **`ISFW` <-> `DCFW`**
* **`NGFW` <-> `Modems`**
* **`NGFW` <-> `MPLS`**

[appendix]
[#miseenpratique]
== Mise en pratique

Dasn cette section nous allons détailler les différentes commandes utilisées pour mettre en place les différents éléments de votre achitecture.

_Nous allons détailler uniquement la partie du site local et non le site distant._

Pour vous fournir ces informations utilisons une simulation fonctionnelle avec une architecture matériel sensiblement différentes mais avec un fonctionnement similaire.

.Schéma packet tracer
image::images/pt.PNG[]

Les liaisons stacks ne sont pas possibles sur des outils de simulations comme Packet Tracer, de plus avec une liaison stack plusieurs équipements physiques deviennent un seul équipement logique. +
Pour cette raison sur ce schéma les switchs des 2 étages du bâtiment B ainsi que le coeur de réseau sont représentés par un switch unique

=== Liaison entre les switchs

Pour les liaison entre les switchs nous utilisons le protocol `LACP` POUR l'agrégation de liens. 

Voici par exemple la configuration à réaliser pour le lien entre `CR` et `Bat C`:

.Activation LACP
[source,cisco]
----
CR#configure terminal
Enter configuration commands, one per line.  End with CNTL/Z.
CR(config)#interface range GigabitEthernet 1/0/6 - 7
CR(config-if-range)#channel-group 1 mode passive

BatC#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
BatC(config)#interface range GigabitEthernet 0/1 - 2
BatC(config-if-range)#channel-group 1 mode active
----

Avec ces commandes le Switch `CR` utilisera le protocol `LACP` pour l'agrégation de lien sur les lien GigaEthernet 1/0/6 à 1/0/7 s'il trouve un équipement désirant utiliser le `LACP` lui aussi, nous disons donc au Swicth `BatC` d'utiliser ce protocol sur les deux liens correspondants. 

Il faut ensuite réaliser les même étapes sur les autres équipements.


=== Vlans

Nous allons ensuite définir les Vlans, activer le VTP pour répliquer la configuration sur les autres swicths et ensuite configurer les interfaces en conséquence.

Nous utilisons la liste proposée dans la section <<Vlans,vlans>> de ce document.

.Création des Vlans
[source,cisco]
----
CR(config)#vlan 10
CR(config-vlan)#name Admins
CR(config-vlan)#vlan 20
CR(config-vlan)#name Network
CR(config-vlan)#vlan 30
CR(config-vlan)#name VOIP
CR(config-vlan)#vlan 40
CR(config-vlan)#name Serveurs
CR(config-vlan)#vlan 50
CR(config-vlan)#name PC
CR(config-vlan)#vlan 60
CR(config-vlan)#name Imprimantes
CR(config-vlan)#vlan 70
CR(config-vlan)#name Wifi
CR(config-vlan)#vlan 80
CR(config-vlan)#name invites
CR(config-vlan)#vlan 100
CR(config-vlan)#name NATIVE
----

.Activation du Serveur VTP sur `CR`
[source,cisco]
----
CR(config)#vtp domain tp.local
CR(config)#vtp password superpassword
CR(config)#vtp mode server
----

.Activation du Client VTP sur `Bat C`
[source,cisco]
----
BatC(config)#vtp domain tp.local
BatC(config)#vtp password superpassword
BatC(config)#vtp mode client
----

_Dans le cas du switch `Bat B étage 1`, le mode vtp sera transparent de façon à prendre en compte la configuration vlan tout en permettant à `Bat B étage 2` de la récupérer également._


Pour la configuration des interfaces nous allons commencer par les liaisons `trunk`, ces liaisons se trouvant entre les switchs permettent de transporter plusieurs Vlans.

.Trunk `CR` - `Bat C`
[source,cisco]
----
CR(config)#int port-channel 1
CR(config-if)#switchport trunk encapsulation dot1q
CR(config-if)#switchport trunk allowed vlan 100,10,20,30,40,50,60,70,80
CR(config-if)#switchport trunk native vlan 100
CR(config-if)#switchport mode trunk

BatC(config-if)#int po1
BatC(config-if)#switchport trunk allowed vlan 100,10,20,30,40,50,60,70,80
BatC(config-if)#switchport trunk native vlan 100
BatC(config-if)#switchport mode trunk
----

_Le système Cisco IOS permet de raccourcir les commandes utilisées en tapant uniquement les premières lettres de chaque instruction. Les deux blocs ci-dessus sont donc identitiques sauf pour la déclaration de l'encapsulation qui n'est pas nécessaire sur le model de switch utilisé pour le `Bat C`._


=== Routage inter-vlan

Dans cet exemple afin de permettre la communication entre les vlans nous allons faire un `router on a stick` qui permet au routeur de communiquer avec tous les vlans depuis une interface unique.

Voici un exemple pour quelques uns des Vlans :

.router-on-the-stick
[source,cisco]
----
RDC(config)#int g0/0/0
RDC(config-if)#no ip address
RDC(config-if)#no shutdown 

RDC(config-if)#int g0/0/0.100
RDC(config-subif)#encapsulation dot1Q 100 native
RDC(config-subif)#ip address 10.10.100.254 255.255.255.0
RDC(config-subif)#description NATIVE

RDC(config-if)#int g0/0/0.10
RDC(config-subif)#encapsulation dot1Q 10
RDC(config-subif)#ip address 10.10.10.254 255.255.255.0
RDC(config-subif)#description Admins

RDC(config-subif)#int g0/0/0.20
RDC(config-subif)#encapsulation dot1Q 20
RDC(config-subif)#ip address 10.10.20.254 255.255.255.0
RDC(config-subif)#desc Network

RDC(config-subif)#int g0/0/0.30
RDC(config-subif)#encapsulation dot1Q 30
RDC(config-subif)#ip address 10.10.30.254 255.255.255.0
RDC(config-subif)#desc VOIP

RDC(config-subif)#int g0/0/0.40
RDC(config-subif)#encapsualation dot1Q 40
RDC(config-subif)#ip address 10.10.40.254 255.255.255.0
RDC(config-subif)#desc Serveurs

RDC(config-subif)#int g0/0/0.50
RDC(config-subif)#encapsulation dot1Q 50
RDC(config-subif)#ip address 10.10.50.254 255.255.255.0
RDC(config-subif)#desc PCs


RDC(config-subif)#int g0/0/0.60
RDC(config-subif)#encapsulation dot1Q 60
RDC(config-subif)#ip address 10.10.60.254 255.255.255.0
RDC(config-subif)#desc Imprimantes

RDC(config-subif)#int g0/0/0.70
RDC(config-subif)#desc Wifi
RDC(config-subif)#encapsulation dot1Q 70
RDC(config-subif)#ip address 10.10.70.254 255.255.255.0

RDC(config-subif)#int g0/0/0.80
RDC(config-subif)#desc invites
RDC(config-subif)#encapsulation dot1Q 80
RDC(config-subif)#ip address 10.10.80.254 255.255.255.0
----

=== Spanning tree

Dans la situation actuelle il n'y a pas de boucle donc la définition de priorités manuelle n'est pas nécessaire. Nous allons quand même le faire à titre d'exemple. +
Il est quand même important de définir le mode `spanning tree` utilisé et les ports à ignorer afin de gagner en stabilité.

Nous avons choisi d'utiliser le mode `rapid-pvst`, qui permet un négociation plus rapide et le `portfast` pour désactiver le protocole sur les ports en mode `access` (les ports auxquels sont reliés les équipements utilisateurs).

Afin d'éviter une modification de la topologie `spanning tree` dans le cas où un utilisateur raccorderait un switch sur un port access, nous activons le `bpduguard` sur ces mêmes ports.

_Nous n'activons pas le `bpduguard` ou le `portfast` sur les switchs du `CR` et du `DC` car ces équipements ne sont pas reliés à des équipements finaux._

.CR
[source,cisco]
----
CR(config)#spanning-tree mode rapid-pvst 
CR(config)#spanning-tree vlan 10,20,30,40,50,60,70,80,100 priority 0
CR(config)#spanning-tree vlan 10,20,30,40,50,60,70,80,100 root primary
----

.Bat C
[source,cisco]
----
BatC(config)#spanning-tree mo rap
BatC(config)#spanning-tree vlan 10,20,30,40,50,60,70,80,100 priority 4096
BatC(config)#spanning-tree portfast default
BatC(config)#spanning-tree portfast bpduguard default
----

.Bat B étage 1
[source,cisco]
----
BatB1(config)#spanning-tree mode rapid-pvst
BatB1(config)#spanning-tree vlan 10,20,30,40,50,60,70,80,100 priority 8192
BatB1(config)#spanning-tree portfast default
BatB1(config)#spanning-tree portfast bpduguard default
----


.Bat B étage 2
[source,cisco]
----
BatB2(config)#spanning-tree mode rapid-pvst
BatB2(config)#spanning-tree vlan 10,20,30,40,50,60,70,80,100 priority 12288
BatB2(config)#spanning-tree portfast default
BatB2(config)#spanning-tree portfast bpduguard default
----

=== Equipement clients

Maintenant nous allons mettre les différents équipements dans les Vlans appropriés.

.Vlans Bat C
[source,cisco]
----
BatC(config)#int f0/2
BatC(config-if)#switchport access vlan 60
BatC(config-if)#switchport mode access
BatC(config-if)#spanning-tree portfast
BatC(config-if)#spanning-tree bpduguard enable

BatC(config)#interface fastEthernet 0/1
BatC(config-if)#switchport access vlan 50
BatC(config-if)#switchport voice vlan 30 #<1>
BatC(config-if)#switchport mode access
BatC(config-if)#spanning-tree portfast default
BatC(config-if)#spanning-tree portfast bpduguard default
----
<1> Cette instruction permet de raccorder un téléphone sur le switch et d'avoir un autre équipement connecté derrière tout en étant dans des Vlans différents.

=== Configurations

Vous trouverez les configurations des différents équipements dans le dossier config du TP sur le déppôt https://gitlab.com/antoine33520/architecture-reseau[].

Voici les liens vers ces fichiers :

* link:https://gitlab.com/antoine33520/architecture-reseau/-/raw/master/TP1/configs/RDC_startup-config.txt[Routeur DC]
* link:https://gitlab.com/antoine33520/architecture-reseau/-/raw/master/TP1/configs/SDC_startup-config.txt[Switch DC]
* link:https://gitlab.com/antoine33520/architecture-reseau/-/raw/master/TP1/configs/VOIP_startup-config.txt[Serveur VOIP]
* link:https://gitlab.com/antoine33520/architecture-reseau/-/raw/master/TP1/configs/BatC_startup-config.txt[Switch Bat C]
* link:https://gitlab.com/antoine33520/architecture-reseau/-/raw/master/TP1/configs/BatBe1_startup-config.txt[Switch Bat B 1er étage]
* link:https://gitlab.com/antoine33520/architecture-reseau/-/raw/master/TP1/configs/BatBe2_startup-config.txt[Switch Bat B 2eme étage]

[appendix]
== Firewall et Accès distants

Afin de vous présenter le type de configuration que nous souhaitons mettre en place pour votre réseau nous avons préparé une démo fonctionnelle des différents services utilisés.

Pour ce lab nous avons 2 pare-feux Fortigate et un pc distant pour la démonstration d'une connexion type télétravail.

Nous avons créé un réseau sur le pare-feu simulant le datacenter et un autre sur le pare-feu représentant un site distant.

Nous avons configuré les réseau suivants:

* `10.1.40.0/24` correspondant au vlan serveurs de votre datacenter
* `10.5.10.0/24` est utilisé pour simuler un réseau sur le site n°5

=== Configuration du SD-WAN

Comme nous avons à la fois un accès à internet opérateur et un MPLS nous pouvons configurer le SD-WAN afin de définir quel lien sera utilisé en fonction du service :

__Dans cet exemple la carte admin correspond au MPLS.__

Comme le traffic par le MPLS est souvent facturé à la consommation nous définissons un coût différent pour favorisé l'utilisation de la sortie internet standard.

image::images/sdwan.png[]

Nous allons ensuite définir des règles avec par exemple une favorisation du MPLS avec la meilleur qualité pour la VOIP :

image::images/sdwan_voip.png[]

Nous pouvons aussi favorisé l'accès internet standard avec la meilleure bande passante pour les application office 365 :

image::images/sdwan_rules.png[]

=== Confiration du NAT

Le nat va permettre au vlan serveur d'avoir accès à internet en utilisant l'adresse ip publique du pare-feu comme adresse de sortie.

image::images/nat_lan.png[]

Avec cette configuration nous faisons aussi beaucoup d'autres choses :

* Nous n'autorisons que le traffic http, https et dns afin de fournir uniquement un accès à internet, comme il s'agit du réseau des serveurs nous pourrons ajouter le FTP pour certains serveurs de mises à jours. Il est également possible de faire des règles plus ciblées sur des adresses spécifiques
* Les profiles de sécurité, nous les avons laisser en valeur par défaut qui sont déjà très efficaces avec le matériel fortinet :
** L'antivirus, il s'agit d'une fonction qui scan le traffic afin de vérifier d'un virus n'est pas en train de communiquer sur votre réseau et de se propager
** Le filtrage web, cette fonction permet d'analyser le traffic web afin de bloquer les sites potentiellements dangereux ou que vous jugez comme innaproprié, vous pouvez également demander au pare-feu d'autorisé le traffic mais d'enregistrer ces connexions.
** Le filtrage dns va faire la même chose que le filtrage mais au niveau des requêtes dns.
** Le contrôle des applications, grâce à une base de signatures le pare-feu va également annalyser le traffic spécifiques à un grand nombre d'application, il s'agit d'une fonctionnalité de sécurité mais cette liste permet aussi la priorisation des flux.
** La fonction IPS permet de bloquer des connexion selon une liste de failles de sécuritées mise à jour automatiquement depuis les serveurs de fortinet

=== Configuration de l'accès site à site

Afin de permettre à vos sites de communiquer de façon transparente avec le datacenter nous mettons en place un VPN site à site via IPsec.

Dasn un premier temps nous allons configurer le pare-feu du datacenter puis celui du site n°5.

==== Configuration pare-feu datacenter en gui

image::images/ipsec_dc_network.png[]
image::images/ipsec_dc_auth.png[]
image::images/ipsec_dc_p1.png[]
image::images/ipsec_dc_p2.png[]

Ici le mode `Dialup User` va nous permettre de répondre à des demandes de connexion venant de plusieurs sites sur l'adresse IP défini.

La partie `Mode Config` sera configuré plus en détail par la suite avec le terminal.

Pour ces options nous avons choisi le plus haut niveau de chiffrement au niveau du traffic et le hash le plus complexe pour l'authentification.

Pour plus de sécurité pour votre infrastructure grâce à votre autorité de certification active directory nous mettre en place une authenetification par certificats et nous par echange de clé comme c'est le cas dans cet exemple.

==== Configuration du site 5

Pour l'accès d'un site la configuration de la phase 1 doit être identiquement la même, pour la phase 2 se sera la configuration via le terminal qui sera prise en compte.

Il faut par contre modifier la configuration du réseau afin d'indiquer le pare-feu du datacenter comme `hub` pour la connexion.

image::images/ipsec_site5_network.png[]

==== Modification avec le terminal pour OSPF

Afin de rendre permettre d'utiliser du routage dynamique sur l'IPsec nous adaptons la configuration de l'IPsec :

__Dans cet exemple `Sites` représente le nom du VPN IPsec du côté du datacenter et `TP_reseau` pour le site 5.__

.Datacenter
[source, shell]
----
# config vpn ipsec phase1-interface #<1>
    edit "Sites"
        set net-device enable
        set mode-cfg enable
        set add-route disable
        set ipv4-start-ip 10.1.1.4  
        set ipv4-end-ip 10.1.1.255
    next
end
# config vpn ipsec phase2-interface #<2>
    edit "Sites-p2"
        set phase1name "Sites"
    next
end
# config system interface #<3>
    edit "Sites"
        set vdom "root"
        set type tunnel
        set snmp-index 8
        set interface "port1"
    next
end
----
<1> Adaptation de la phase 1
<2> Adaptation de la phase 2
<3> Adaptation de la configuration de l'interface IPsec

.Site 5
[source, shell]
----
# config vpn ipsec phase1-interface #<1>
    edit "TP_reseau"
        set mode-cfg enable
        set add-route disable
    next
end
# config vpn ipsec phase2-interface #<2>
    edit "TP_reseau-PHASE2"
        set phase1name "TP_reseau"
        set auto-negotiate enable
    next
end

# config system interface #<3>
    edit "TP_reseau"
        set vdom "root"
        set type tunnel
        set snmp-index 7
        set interface "port1"
    next
end
----
<1> Adaptation de la phase 1
<2> Adaptation de la phase 2
<3> Adaptation de la configuration de l'interface IPsec

=== Configuration OSPF

Nous configurons ensuite OSPF pour échanger les routes entre les sites :

.Datacenter
[source,shell]
----
# config router ospf
    set router-id 10.1.0.0
# config area
    edit 0.0.0.0
next
end
# config ospf-interface
    edit "1"
        set interface "Sites"
        set cost 10
        set dead-interval 40
        set hello-interval 10
        set network-type point-to-point
    next
end
# config network
    edit 1
        set prefix 10.1.40.0 255.255.255.0
    next
end
# config redistribute "connected"
end
# config redistribute "static"
end
end
----

.Site 5
[source,shell]
----
# config router ospf
    set router-id 10.5.0.0
# config area
    edit 0.0.0.0
next
end
# config ospf-interface
    edit "1"
        set interface "TP_reseau"
        set cost 10
        set dead-interval 40
        set hello-interval 10
        set network-type point-to-point
    next
end
# config network
    edit 1
        set prefix 10.5.10.0 255.255.255.0
    next
end
# config redistribute "connected"
end
# config redistribute "static"
end
end
----

==== Configuration des règles de pare-feu pour OSPF

__Internal correspond à une interface de loopback utilisée uniquement par le pare-feu.__

.Datacenter
[source,shell]
----
# config firewall policy
    edit 3
        set name "12"
        set srcintf "Sites"
        set dstintf "internal"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "OSPF"
    next
    edit 4
        set name "21"
        set srcintf "internal"
        set dstintf "Sites"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "OSPF"
    next
end
----

.Site 5
[source,shell]
----
# config firewall policy
    edit 2
        set name "21"
        set srcintf "internal"
        set dstintf "TP_reseau"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "OSPF"
    next
    edit 3
        set name "12"
        set srcintf "TP_reseau"
        set dstintf "internal"
        set srcaddr "all"
        set dstaddr "all"
        set action accept
        set schedule "always"
        set service "OSPF"
    next
end
----

==== Vérification des routes OSPF

Nous pouvons vérifier que le datacenter récupère bien les routes du sites 5:

[source,shell]
----
fortigate # get router info routing-table ospf
Routing table for VRF=0
O E2    10.5.10.0/24 [110/10] via 10.254.0.5, Sites_0, 00:03:34
O E2    192.168.1.0/24 [110/10] via 10.254.0.5, Sites_0, 00:03:34
----

==== Ajouts de réseaux supplémentaires OSPF

Nous pouvons maintenant ajouter des réseaux supplémentaires depuis l'interface graphique pour les rendes disponibles dans notre OSPF:

image::images/ospf_networks.png[Ajouts de réseaux dasn OSPF]

=== Pare-feu IPsec

Afin nous allons créer une règle de pare-feu pour la traffic IPsec en utilisant le site distant comme adresse de destination et le réseau serveur (ici lan) comme source, pour un plus grand nombre de réseaux nous utiliserons des groupes afin de ne pas ajouter chaque destination et source manuellement.

Cette règle possède également moins de vérification au niveau du traffic sachant qu'il s'agit uniquement de traffic interne sans point de sorti vers internet.

Nous activons l'accès au divers services de votre datacenter via des groupes de services pour une configuration plus efficace et plus maintenable de vos équipements.

image::images/Policy_datacenter_ipsec_sites1.png[]

=== Résumé des règles de pare-feu

Voici les divers règles de pare-feu en application pour cette démonstration.

Vous pouvez voir que grâce à une fonctionnalité de fortigate en une seule action nous avons créé la règle inverse de la dernière crée ci dessus :

image::images/firewall_dc_policy.png[]

=== VPN SSL

Ensuite nous passons à la mise en place du VPN SSL qui va permettre aux utilisateurs distants de se connecter aux ressources de l'entreprise depuis n'importe quelle connexion internet.

Dans un premier temps il faut définir le type de portail du sera utiliser par les utilisateurs :

image::images/vpn_ssl_portal.png[]

Le `split tunneling` permet que le traffic venant des clients ne passe par le pare-feu uniquement pour joindre les ressources définies (ici le réseau des serveurs (lan net) et des serveurs sensibles).
Il est également possible d'utiliser le même système au niveau des serveurs dns utilisés, n'ayant pas de serveurs dns dans cette exemple nous n'activons pas l'option.

Nous activons également l'interface web afin de permettre la connexion des ressources de l'entreprise depuis un portail web ouvrant directement des connexion vers les ressources demandées au lien d'avoir un client lourd établissent une connexion vpn.

Nous pouvons ensuite configurer le VPN :

image::images/vpn_ssl_conf.png[]

Pour vous cas précis ce que nos ferons c'est changé le port de l'interface de gestion et nous définirons le port du VPN SSL sur 443 pour une utilisations plus simple pour vos employés. Ensuite grace à votre autorité de certification nous pourrons définir un certificat ssl valable sur les ordinateurs de vos employés.

Pour des raisons de sécurité nous avons défini un groupe pour les utilisateur autorisés à utiliser le VPN, les utilisateurs non présents dans ce groupe ne pourrons pas l'utiliser.

Ensuite nous avons défini les règles de pare-feu appropriées pour autoriser le traffic vers le datacenter depuis le vpn:

image::images/ssl_rule_policy.png[]

==== Démonstration

Les deux moyens de connexion :

===== Le portail :

image::images/vpn_ssl_web.png[]

Qui va permettre de se connecter directement à une ressource ou une machine. Il est également possible de définir des favoris dans la configuration pour qu'ils soient présent sur le portail de chaque utilisateur.

===== Le client :

image::images/vpn_ssl_client.png[]

Nous voyons que l'utilisateur `quentin` est bien connecté au vpn et peut maintenant communiquer avec le datacenter.